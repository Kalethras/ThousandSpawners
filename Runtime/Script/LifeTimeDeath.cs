﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeTimeDeath : MonoBehaviour
{
    public float m_time=5;
    void Start()
    {
        Destroy(this.gameObject, m_time);
    }
    
}
