﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassicSpawn : MonoBehaviour
{
    public GameObject m_prefab;
    public float m_timeBetwen=1;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(m_timeBetwen);
            GameObject gamo = Instantiate(m_prefab);
            gamo.transform.position = transform.position;
            gamo.transform.rotation = transform.rotation;
            gamo.transform.localScale = Vector3.one * 0.1f;
            gamo.SetActive(true);
        }
        
    }
    
}
