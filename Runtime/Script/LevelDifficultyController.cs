﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDifficultyController : MonoBehaviour
{
    public ClassicSpawn m_spawn;
    public AnimationCurve m_timeSpawnDifficulty;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        m_spawn.m_timeBetwen =  m_timeSpawnDifficulty.Evaluate(Time.time / 60f);
    }
}
