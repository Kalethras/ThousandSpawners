﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnThatBitch : MonoBehaviour
{
    public float m_timeBetweenWaves = 10f;
    private float timer;
    public GameObject m_objectToSpawn;
    public GameObject m_spawnAnchor;
    public List<GameObject> m_spawnerList = new List<GameObject>();



    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > m_timeBetweenWaves)
        {
            timer = 0;
            int spawnNumber = UnityEngine.Random.Range(1, 6);
            
            ChooseSpawner(spawnNumber);

            Debug.Log("chiffre random = " + spawnNumber);
        }
    }

    private void ChooseSpawner(int spawnNumber)
    {
        switch (spawnNumber)
        {
            case 1:
                SpawnThemAll(m_spawnerList[0]);
                break;
            case 2:
                SpawnThemAll(m_spawnerList[1]);
                break;
            case 3:
                SpawnThemAll(m_spawnerList[2]);
                break;
            case 4:
                SpawnThemAll(m_spawnerList[3]);
                break;
            case 5:
                SpawnThemAll(m_spawnerList[4]);
                break;
            case 6:
                SpawnThemAll(m_spawnerList[5]);
                break;
        }
        Debug.Log("spawner utilisé = " + spawnNumber);
    }

    private void SpawnThemAll(GameObject spawner)
    {
        foreach(Transform child in spawner.transform)
        {
            GameObject objectSpawned = Instantiate(m_objectToSpawn, transform.position, transform.rotation);
            Debug.Log("Je spawn un objet");
        }
    }
}
